package org.bitbucket.bradleysmithllc.java_cl_parser;

@java.lang.annotation.Target({java.lang.annotation.ElementType.TYPE})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface CLIOptionSet
{
	enum set_type
	{
		/**
		 * Means that this option set is a subset of the actual arguments.
		 */
		subset_of_args,
		/**
		 * Means that this option set must EXACTLY match the actual supplied arguments.
		 */
		exact_match
	}

	/**
	 * The identifier for this rule set.  Used in exception messages.
	 */
	String id();

	/**
	 * A list of the short parameter names which this set consists of.
	 * @return
	 */
	String [] optionShortNames();

	/**
	 * The message to display when this set is matched.
	 * @return
	 */
	String description();

	/**
	 * The type of this option set.
	 * @return
	 */
	set_type setType() default set_type.exact_match;
}