package org.bitbucket.bradleysmithllc.java_cl_parser;

public class MissingCLIEntryException extends Exception
{
	public MissingCLIEntryException()
	{
	}

	public MissingCLIEntryException(String message)
	{
		super(message);
	}

	public MissingCLIEntryException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public MissingCLIEntryException(Throwable cause)
	{
		super(cause);
	}
}