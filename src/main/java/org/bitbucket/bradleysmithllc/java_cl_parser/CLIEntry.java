package org.bitbucket.bradleysmithllc.java_cl_parser;

@java.lang.annotation.Target({java.lang.annotation.ElementType.TYPE})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface CLIEntry
{
	enum parser_type
	{
		gnu,
		posix,
		basic
	}

	/**
	 * The description of this tool.  This should provide enough information that a user will know
	 * how to use the tool without having to run through trial and error.
	 * @return
	 */
	String description();

	/**
	 * A simple organizational name for this command.
	 * @return
	 */
	String commandGroup() default CLIOption.NULL_STRING_VALUE;

	/**
	 * The short name for this tool.  This is only used in the generated documentation.  The default
	 * is the simple class name as returned by Class.getSimpleName().
	 * @return
	 */
	String nickName() default CLIOption.NULL_STRING_VALUE;

	/**
	 * This determines the exact syntax that is applied to the command line. These parser types match
	 * the Apache Commons CLI parsers.
	 * @return
	 */
	parser_type parserType() default parser_type.gnu;

	/**
	 * Identifier for the maintainer of this tool.  Could be an email address, messenger name, whatever.
	 * @return
	 */
	String contact() default CLIOption.NULL_STRING_VALUE;

	/**
	 * Where this project documentation lives.
	 * @return
	 */
	String documentationUrl() default CLIOption.NULL_STRING_VALUE;

	/**
	 * Identifier for where this project lives in version control.  There are no rules,
	 * it just needs to be clear.  E.G., for subversion ideally
	 * this would be a fully-qualified subversion URL.
	 * @return
	 */
	String versionControl() default CLIOption.NULL_STRING_VALUE;

	/**
	 * The current version number.  Required.
	 * @return
	 */
	String version();

	/**
	 * A list of option sets which are valid for this command.  If specified, the options specified must match
	 * one of these sets according to the set type or be considered invalid.  If not specified, any option combinations are considered valid.
	 * This can be used in combination with invalidarameterSets as long as none of the sets overlap.
	 * @return
	 */
	CLIOptionSet [] validarameterSets() default {};

	/**
	 * A list of option sets which are invalid for this command.  If specified, any combination of options which
	 * match one of these sets according to the set type will be considered invalid.  This can be used in combination with validarameterSets as long
	 * as none of the sets overlap.
	 * @return
	 */
	CLIOptionSet [] invalidarameterSets() default {};

	/**
	 * If provided, the specific order to assign the options in.  The names here must exactly match either the
	 * name or long name of a declared option.  Any option names not specified will follow the default rule
	 * of declaration order after the explicitly-ordered options are set.
	 * @return
	 */
	String [] assignmentOrder() default {};
}