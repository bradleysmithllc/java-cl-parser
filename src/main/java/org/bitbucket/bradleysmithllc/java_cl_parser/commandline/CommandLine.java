package org.bitbucket.bradleysmithllc.java_cl_parser.commandline;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import java.util.HashMap;
import java.util.Map;

public class CommandLine {
	private final OptionSet optionSet;
	private final Map<Option, String> options = new HashMap<>();

	public CommandLine(OptionSet optionSet, String... commandLine) throws ParseException {
		this.optionSet = optionSet;

		Option thisOption = null;

		int unnamedOrdinalPosition = 1;

		// parse this as anything beginning with '-' is an option
		for (String nextValue : commandLine) {
			if (thisOption != null) {
				// option is (potentially) awaiting a value
				// check for a flag.  If this is a flag,
				// an optional argument of true/false can be supplied - nothing else.
				// Any other occurrence is treated as --flag --nextflag
				if (thisOption.flag()) {
					if (nextValue.equalsIgnoreCase("true") || nextValue.equalsIgnoreCase("false")) {
						// value has been overridden
						addOutputOption(options, thisOption, nextValue.toLowerCase());
						thisOption = null;
						continue;
					} else {
						// save the flag as 'true' and process this value as an option
						addOutputOption(options, thisOption, "true");
						thisOption = null;
					}
				} else {
					// not a flag.  Grab whatever next value and save it
					addOutputOption(options, thisOption, nextValue);
					thisOption = null;
					continue;
				}
			}

			if (nextValue.startsWith("-")) {
				// this is a short name or a long name
				if (nextValue.startsWith("--")) {
					// this is a long name.
					thisOption = optionSet.byLongName(nextValue.substring(2));
				} else {
					// short name
					thisOption = optionSet.byShortName(nextValue.substring(1));
				}

				if (thisOption == null) {
					throw new ParseException("Option not defined '" + nextValue + "'");
				}
			} else {
				// This can be a positional argument.  Check if this ordinal is defined
				Option option = optionSet.byUnnamedArgumentOrdinal(unnamedOrdinalPosition++);

				if (option != null) {
					addOutputOption(options, option, nextValue);
				}
			}
		}

		// end of input.  This can be a flag, which is okay, or it can be nothing
		if (thisOption != null) {
			if (thisOption.flag()) {
				addOutputOption(options, thisOption, "true");
			} else {
				throw new ParseException("Option dangling wanting value '" + thisOption.longName() + "'");
			}
		}

		// validate any required options
		for (Option option : optionSet.options()) {
			if (!option.optional() && !options.containsKey(option)) {
				throw new ParseException("Required option missing '" + option.longName() + "'");
			}
		}
	}

	private static void addOutputOption(Map<Option, String> options, Option option, String value) {
		// make sure this option was not specified as a flag already
		if (options.containsKey(option)) {
			throw new IllegalArgumentException("Option [" + option.longName() + "] already specified as a flag");
		}

		options.put(option, value);
	}

	public boolean optionSpecified(CLIOption option) {
		return optionSpecified(option.longName());
	}

	public String getOptionValue(CLIOption option) {
		return getOptionValue(option.longName());
	}

	/* This can reference the long or short name */
	public boolean optionSpecified(String name) {
		return options.containsKey(optionSet.byShortOrLongName(name));
	}

	/* This can reference the long or short name */
	public String getOptionValue(String name) {
		return options.get(optionSet.byShortOrLongName(name));
	}

	/* This can reference the long or short name */
	public boolean getFlagValue(String name) {
		return options.get(optionSet.byShortOrLongName(name)).equalsIgnoreCase("true");
	}
}