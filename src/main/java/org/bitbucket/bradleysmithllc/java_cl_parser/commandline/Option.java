package org.bitbucket.bradleysmithllc.java_cl_parser.commandline;

public interface Option {
	int UNLIMITED_VALUES = -1;

	String shortName();
	String longName();
	int unnamedArgumentOrdinal();

	boolean flag();
	boolean optional();
}