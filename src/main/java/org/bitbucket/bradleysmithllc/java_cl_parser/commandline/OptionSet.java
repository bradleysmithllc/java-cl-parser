package org.bitbucket.bradleysmithllc.java_cl_parser.commandline;

import java.util.List;

public interface OptionSet {
	List<Option> options();
	Option byShortName(String name);
	Option byLongName(String longName);
	Option byUnnamedArgumentOrdinal(int ordinal);
	default Option byShortOrLongName(String name) {
		if (byShortName(name) != null) {
			return byShortName(name);
		} else {
			return byLongName(name);
		}
	}
}