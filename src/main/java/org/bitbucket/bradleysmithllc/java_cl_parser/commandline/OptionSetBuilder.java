package org.bitbucket.bradleysmithllc.java_cl_parser.commandline;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import java.util.ArrayList;
import java.util.List;

public class OptionSetBuilder {
	private static final class OptionImpl implements Option {
		String shortName;
		String longName;
		boolean isFlag;
		int unnamedOrdinal = CLIOption.UNSPECIFIED_ORDINAL;
		boolean isOptional;

		@Override
		public String shortName() {
			return shortName;
		}

		@Override
		public String longName() {
			return longName;
		}

		@Override
		public int unnamedArgumentOrdinal() {
			return unnamedOrdinal;
		}

		@Override
		public boolean flag() {
			return isFlag;
		}

		@Override
		public boolean optional() {
			return isOptional;
		}
	}

	public OptionSetBuilder option(CLIOption option) {
		option(option.longName())
			.optional(!option.required())
			.shortName(option.name())
			.unnamedOrdinal(option.unnamedArgumentOrdinal());

		// in the case of arguments with values, get the value cardinalities worked out
		if (option.valueType() == CLIOption.value_type.flag)
		{
			flag(true);
		}
		else
		{
			flag(false);
		}

		return this;
	}

	private OptionImpl thisOption;
	private List<Option> builtOptions = new ArrayList<>();

	public OptionSetBuilder option(String longName) {
		if (thisOption != null) {
			builtOptions.add(thisOption);
		}

		thisOption = new OptionImpl();
		thisOption.shortName = longName;
		thisOption.longName = longName;

		return this;
	}

	public OptionSetBuilder unnamedOrdinal(int ordinal) {
		thisOption.unnamedOrdinal = ordinal;
		return this;
	}

	public OptionSetBuilder shortName(String shortName) {
		thisOption.shortName = shortName;
		return this;
	}

	public OptionSetBuilder flag(boolean flag) {
		thisOption.isFlag = flag;
		return this;
	}

	public OptionSetBuilder optional(boolean opt) {
		thisOption.isOptional = opt;
		return this;
	}

	public OptionSet build() {
		if (thisOption != null) {
			builtOptions.add(thisOption);
		}

		return new OptionSet() {
			@Override
			public List<Option> options() {
				return builtOptions;
			}

			@Override
			public Option byShortName(String name) {
				for (Option option : options()) {
					if (option.shortName().equalsIgnoreCase(name)) {
						return option;
					}
				}

				return null;
			}

			@Override
			public Option byLongName(String longName) {
				for (Option option : options()) {
					if (option.longName().equalsIgnoreCase(longName)) {
						return option;
					}
				}

				return null;
			}

			@Override
			public Option byUnnamedArgumentOrdinal(int ordinal) {
				for (Option option : options()) {
					if (option.unnamedArgumentOrdinal() == ordinal) {
						return option;
					}
				}

				return null;
			}
		};
	}
}