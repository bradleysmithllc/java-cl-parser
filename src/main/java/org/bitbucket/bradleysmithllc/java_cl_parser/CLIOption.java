package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.bitbucket.bradleysmithllc.java_cl_parser.commandline.Option;

@java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface CLIOption
{
	/**
	 * Value for no ordinal.
	 */
	int UNSPECIFIED_ORDINAL = -1;

	/**
	 * Literal which can be used to test the annotations for a 'null', or non-user specified value.
	 */
	public static final String NULL_STRING_VALUE = "\0";

	/**
	 * Literal which holds the default value for an array separator.
	 */
	public static final char ARRAY_SEPARATOR_DEFAULT = ',';

	/**
	 * Literal which holds the escape character.
	 */
	public static final char ESCAPE_CHARACTER_DEFAULT = '\\';

	/**
	 * Literal which holds the escape character.
	 */
	public static final String ESCAPE_CHARACTER_DEFAULT_STRING = "\\";

	enum value_type
	{
		required,
		flag
	}

	int UNLIMITED_VALUES = Option.UNLIMITED_VALUES;

	/** The unique name this option is known by.  Can also be the 'short form' as compared to the longName.
	 *
	 * @return
	 */
	String name();

	/**
	 * Default is CLIOption.NULL_STRING_VALUE, which means no long name.  If provided, it must be unique.
	 *
	 * @return
	 */
	String longName() default NULL_STRING_VALUE;

	String description() default "No description provided";

	/* If arguments are provided without flags or names, this indicates the position to use for this one.
	 *  E.G., if an option has a name 'path' and position 1, then the invocation <cmd> --path /p and <cmd> /p
	 *  would accomplish the same thing.  This is one-based. */
	int unnamedArgumentOrdinal() default UNSPECIFIED_ORDINAL;

	/**
	 * Default is false
	 *
	 * @return
	 */
	boolean required() default false;

	/**
	 * default is value_type.required
	 *
	 * @return
	 */
	value_type valueType() default value_type.required;

	/**
	 * Default is 1.  UNLIMITED_VALUES means any; any specific number means that exact number of arguments
	 *
	 * @return
	 */
	int valueCardinality() default 1;

	/**
	 * Defines char used to separate list entries in a value list.  Default is a comma.
	 *
	 * @return
	 */
	char valueSeparator() default ARRAY_SEPARATOR_DEFAULT;

	/**
	 * Default is CLIOption.NULL_STRING_VALUE which means no default.
	 *
	 * @return
	 */
	String defaultValue() default NULL_STRING_VALUE;

	/**
	 * Defines a list of values that represent the valid argument values.
	 * CLIOption.NULL_STRING_VALUE represents no enumerated values.
	 * @return
	 */
	String [] enumeratedValues() default {};
}