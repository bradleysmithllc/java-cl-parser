package org.bitbucket.bradleysmithllc.java_cl_parser;

public class CapturingShutdownHandler extends ShutdownHandler
{
	private Integer exit_code;

	@Override
	public synchronized Object shutdown(int code) {
		exit_code = new Integer(code);
		return null;
	}

	public Integer getExitCode() {
		return exit_code;
	}
}