package org.bitbucket.bradleysmithllc.java_cl_parser;

public class InvalidCLIEntryException extends Exception
{
	public enum invalid_cause
	{
		bad_option_set,
		bad_options,
		bad_option_assignment_order,
		missing_cli_main,
		redundant_cli_main,
		class_access_error
	}

	private final invalid_cause invalidCause;

	public InvalidCLIEntryException(invalid_cause cause)
	{
		invalidCause = cause;
	}

	public InvalidCLIEntryException(String message, invalid_cause cause)
	{
		super(message);

		invalidCause = cause;
	}

	public InvalidCLIEntryException(String message, Throwable cause, invalid_cause icause)
	{
		super(message, cause);

		invalidCause = icause;
	}

	public InvalidCLIEntryException(Throwable cause, invalid_cause icause)
	{
		super(cause);

		invalidCause = icause;
	}
}