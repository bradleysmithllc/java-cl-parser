package org.bitbucket.bradleysmithllc.java_cl_parser;

public interface CLParserContext {
	CLParameter parameter(String longName);
}