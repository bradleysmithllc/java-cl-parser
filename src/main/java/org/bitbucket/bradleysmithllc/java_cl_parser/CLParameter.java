package org.bitbucket.bradleysmithllc.java_cl_parser;

public interface CLParameter {
	boolean wasSpecified();
	String rawValue();
}
