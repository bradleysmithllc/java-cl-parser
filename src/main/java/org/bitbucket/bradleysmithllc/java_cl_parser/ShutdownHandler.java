package org.bitbucket.bradleysmithllc.java_cl_parser;

public class ShutdownHandler {
	private static ShutdownHandler instance = new ShutdownHandler();
	private static ShutdownHandler runInstance = new ShutdownHandler();

	public static ShutdownHandler instance() {
		return runInstance != null ? runInstance : instance;
	}

	public synchronized Object shutdown(int code) {
		// default implementation
		System.exit(code);
		return null;
	}

	public static void resetInstance()
	{
		runInstance = null;
	}

	public static void setInstance(ShutdownHandler newInstance) {
		// (probably also check that this is only called in a test environment)
		runInstance = newInstance;
	}
}