package org.bitbucket.bradleysmithllc.java_cl_parser;

import java.util.ArrayList;
import java.util.List;

//	${optline.shortName} |${optline.longName} |${optline.dataType} |${optline.defaultValue} |${optline.description}
public class OptionLine {
	private String shortName;
	private String longName;
	private String dataType;
	private String defaultValue;
	private boolean required;

	private final List<String> descriptionLines = new ArrayList<String>();

	public List<String> getDescriptionLines()
	{
		return descriptionLines;
	}

	public boolean isRequired()
	{
		return required;
	}

	public void setRequired(boolean required)
	{
		this.required = required;
	}

	public void setDescriptionLines(List<String> lines)
	{
		descriptionLines.addAll(lines);
	}

	public String getShortName()
	{
		return shortName;
	}

	public void setShortName(String shortName)
	{
		this.shortName = shortName;
	}

	public String getLongName()
	{
		return longName;
	}

	public void setLongName(String longName)
	{
		this.longName = longName;
	}

	public String getDataType()
	{
		return dataType;
	}

	public void setDataType(String dataType)
	{
		this.dataType = dataType;
	}

	public String getDefaultValue()
	{
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue)
	{
		this.defaultValue = defaultValue;
	}
}