package org.bitbucket.bradleysmithllc.java_cl_parser.util;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;
import java.util.Map;

public class RecursiveMap {
	private final Map<String, Object> backingStore;

	private final VelocityContext context;
	private final VelocityEngine velocityEngine;
	private	StringWriter w;

	public RecursiveMap(Map<String, Object> backingStore)
	{
		this.backingStore = backingStore;

		context = new VelocityContext(backingStore);

		velocityEngine = new VelocityEngine();

		velocityEngine.init();
	}

	public synchronized Object get(String val)
	{
		Object eval = backingStore.get(val);

		if (eval instanceof String)
		{
			String res = (String) eval;
			String lastRes = res;

			// check to see if any update occurred.  If so, run until there are no more changes.
			do
			{
				lastRes = res;
				res = get0(res);
			}
			while (!res.equals(lastRes));

			return res;
		}

		return eval;
	}

	private synchronized String get0(String val)
	{
		w = new StringWriter();

		velocityEngine.evaluate(context, w, "log", val);

		return w.toString();
	}
}