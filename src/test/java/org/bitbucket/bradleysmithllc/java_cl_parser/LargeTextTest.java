package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

public class LargeTextTest
{
	@Test
	public void bible() throws IOException
	{
		compare("bible", 36502);
	}

	void compare(String file, int lines) throws IOException
	{
		String text = IOUtils.toString(getClass().getClassLoader().getResource(file + ".txt"));

		text = text.replace("\n", "\r");

		List<String> list = StringUtil.wrapTextToList(text, 120);

		/*
		File fout = new File("target", file + "_processed.txt");
		PrintWriter pwr = new PrintWriter(new FileWriter(fout));

		for (String line : list)
		{
			pwr.println(line);
		}

		pwr.close();
		 */

		URL url = getClass().getClassLoader().getResource(file + "_processed.txt");

		BufferedReader br = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));

		String line;

		int offset = 0;

		while ((line = br.readLine()) != null)
		{
			Assert.assertEquals(line, list.get(offset++));
		}

		Assert.assertEquals(lines, offset);
	}
}