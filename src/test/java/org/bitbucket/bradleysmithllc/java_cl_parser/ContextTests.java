package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

@CLIEntry(
		nickName = "run",
		description = "I do lot's of things",
		version = "1.0"
)
public class ContextTests
{
	public CLParserContext context;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@CLIOption(
				name = "p1",
				longName = "p-1"
	)
	public void p1(int count)
	{
	}

	@CLIOption(
			name = "p2",
			longName = "p-2"
	)
	public void p2(int count)
	{
	}

	@CLIContext
	public void context(CLParserContext context)
	{
		this.context = context;
	}

	@CLIMain
	public void main()
	{
	}

	@Test
	public void testNoParameters() throws Throwable
	{
		ContextTests basicTest = new ContextTests();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{});

		Assert.assertFalse(basicTest.context.parameter("p-1").wasSpecified());
		Assert.assertFalse(basicTest.context.parameter("p-2").wasSpecified());
	}
}