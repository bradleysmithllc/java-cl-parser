package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

@CLIEntry(
	nickName = "run",
	description = "DEATH be not proud, though some have called thee Mighty and dreadfull, for, thou art not so, For, those, whom thou think'st, thou dost overthrow, Die not, poore death, nor yet canst thou kill me.  From rest and sleepe, which but thy pictures bee, Much pleasure, then from thee, much more must flow, And soonest our best men with thee doe goe, Rest of their bones, and soules deliverie.  Thou art slave to Fate, Chance, kings, and desperate men, And dost with poyson, warre, and sicknesse dwell, And poppie, or charmes can make us sleepe as well, And better then thy stroake; why swell'st thou then; One short sleepe past, wee wake eternally, And death shall be no more; death, thou shalt die.",
	contact = "bob@apple.com",
	documentationUrl = "http://wiki.com/help",
	versionControl = "svn:svc:http://wagada",
	version = "1.0"
)
public class HelpRequiredTest extends ShutdownMethodsBase
{
	@CLIOption(
		name = "tc",
		longName = "thread-count",
		required = true,
		valueType = CLIOption.value_type.flag
	)
	public void setThreadCount(int count)
	{
	}

	@CLIMain
	public void main()
	{
	}

	@Test
	public void helpMissingRequired() throws Throwable
	{
		HelpRequiredTest basicTest = new HelpRequiredTest();
		CommonsCLILauncher.mainWithInstanceClean(basicTest, new String[]{"-?"});
		assertExitCode(0);
	}
}