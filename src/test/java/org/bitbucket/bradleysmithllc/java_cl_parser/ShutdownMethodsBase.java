package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

/**
 * Created with IntelliJ IDEA.
 * User: bsmith
 * Date: 7/30/13
 * Time: 9:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class ShutdownMethodsBase {
	protected CapturingShutdownHandler capturingShutdownHandler = new CapturingShutdownHandler();

	@Before
	public void installShutdownHandler()
	{
		ShutdownHandler.setInstance(capturingShutdownHandler);
	}

	public void
	assertExitCode(int code)
	{
		Assert.assertNotNull(capturingShutdownHandler);
		Assert.assertEquals(code, capturingShutdownHandler.getExitCode().intValue());
	}

	@After
	public void resetHandler()
	{
		ShutdownHandler.resetInstance();
	}
}
