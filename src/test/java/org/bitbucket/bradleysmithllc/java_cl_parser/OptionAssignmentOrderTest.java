package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Tests that the options are set in the order they are declared regardless of dependencies.
 */
@CLIEntry(
		description = "",
		version = "1.0",
		assignmentOrder = {
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"
		}
)
public class OptionAssignmentOrderTest
{
	private String order = "";
	private Map<String, String> depStr = new HashMap<String, String>();

	@CLIOption(
			name = "a",
			defaultValue = "a${b}"
	)
	public void setA(String val)
	{
		order += "a";
		depStr.put("a", val);
	}

	@CLIOption(
			name = "b",
			defaultValue = "b${c}"
	)
	public void setB(String val)
	{
		order += "b";
		depStr.put("b", val);
	}

	@CLIOption(
			name = "c",
			defaultValue = "c${d}"
	)
	public void setC(String val)
	{
		order += "c";
		depStr.put("c", val);
	}

	@CLIOption(
			name = "d",
			defaultValue = "d${e}"
	)
	public void setD(String val)
	{
		order += "d";
		depStr.put("d", val);
	}

	@CLIOption(
			name = "e",
			defaultValue = "e${f}"
	)
	public void setE(String val)
	{
		order += "e";
		depStr.put("e", val);
	}

	@CLIOption(
			name = "f",
			defaultValue = "f${g}"
	)
	public void setF(String val)
	{
		order += "f";
		depStr.put("f", val);
	}

	@CLIOption(
			name = "g",
			defaultValue = "g${h}"
	)
	public void setG(String val)
	{
		order += "g";
		depStr.put("g", val);
	}

	@CLIOption(
			name = "h",
			defaultValue = "h${i}"
	)
	public void setH(String val)
	{
		order += "h";
		depStr.put("h", val);
	}

	@CLIOption(
			name = "i",
			defaultValue = "i${j}"
	)
	public void setI(String val)
	{
		order += "i";
		depStr.put("i", val);
	}

	@CLIOption(
			name = "j",
			defaultValue = "j"
	)
	public void setJ(String val)
	{
		order += "j";
		depStr.put("j", val);
	}

	@Test
	public void optionsAssignedInDeclarationOrderImplicit() throws Throwable
	{
		OptionAssignmentOrderTest basicTest = new OptionAssignmentOrderTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{});
		Assert.assertEquals("abcdefghij", basicTest.order);

		Assert.assertEquals("abcdefghij", basicTest.depStr.get("a"));
		Assert.assertEquals("bcdefghij", basicTest.depStr.get("b"));
		Assert.assertEquals("cdefghij", basicTest.depStr.get("c"));
		Assert.assertEquals("defghij", basicTest.depStr.get("d"));
		Assert.assertEquals("efghij", basicTest.depStr.get("e"));
		Assert.assertEquals("fghij", basicTest.depStr.get("f"));
		Assert.assertEquals("ghij", basicTest.depStr.get("g"));
		Assert.assertEquals("hij", basicTest.depStr.get("h"));
		Assert.assertEquals("ij", basicTest.depStr.get("i"));
		Assert.assertEquals("j", basicTest.depStr.get("j"));
	}

	@Test
	public void optionsAssignedInDeclarationOrderRegardlessOfCL() throws Throwable
	{
		OptionAssignmentOrderTest basicTest = new OptionAssignmentOrderTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-j", "1", "-a", "0${b}"});
		Assert.assertEquals("abcdefghij", basicTest.order);

		Assert.assertEquals("0bcdefghi1", basicTest.depStr.get("a"));
		Assert.assertEquals("bcdefghi1", basicTest.depStr.get("b"));
		Assert.assertEquals("cdefghi1", basicTest.depStr.get("c"));
		Assert.assertEquals("defghi1", basicTest.depStr.get("d"));
		Assert.assertEquals("efghi1", basicTest.depStr.get("e"));
		Assert.assertEquals("fghi1", basicTest.depStr.get("f"));
		Assert.assertEquals("ghi1", basicTest.depStr.get("g"));
		Assert.assertEquals("hi1", basicTest.depStr.get("h"));
		Assert.assertEquals("i1", basicTest.depStr.get("i"));
		Assert.assertEquals("1", basicTest.depStr.get("j"));
	}
}