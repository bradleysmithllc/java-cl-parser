package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.bitbucket.bradleysmithllc.java_cl_parser.support.EntrySub0;
import org.bitbucket.bradleysmithllc.java_cl_parser.support.EntrySub1;
import org.junit.Assert;
import org.junit.Test;

public class StackTraceResolutionTest
{
	/**
	 * In this test, there are no CLIEntries on the stack. This must fail.
	 */
	@Test(expected = MissingCLIEntryException.class)
	public void noSuitableTrace() throws Throwable
	{
		CommonsCLILauncher.main(new String[]{});
	}

	/**
	 * Two classes in the stack implement CLIEntry, the one closest to the top must be called (EntrySub0)
	 */
	@Test
	public void manySuitableTraces() throws Throwable
	{
		Assert.assertTrue(EntrySub1.pass() instanceof EntrySub0);
	}

	/**
	 * The first class in the stack is a private type which cannot be instantiated.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void ineligibleClassesOnStack() throws Throwable
	{
		@CLIEntry(
				description = "",
				version = "1.0"
		)
		class Ineligible implements Runnable
		{
			Object kick() throws Throwable
			{
				return EntrySub1.pass();
			}

			public void run()
			{
				try
				{
					Assert.assertTrue(CommonsCLILauncher.main(new String[]{}) instanceof EntrySub0);
				}
				catch (Throwable e)
				{
					throw new IllegalArgumentException(e);
				}
			}

			public void main()
			{
				Assert.fail();
			}
		}

		Assert.assertTrue(new Ineligible().kick() instanceof EntrySub0);
		EntrySub0.passThis(new Ineligible());
	}

	/**
	 * The first class in the stack is a private type which cannot be instantiated and is not a subtype of CLIEntry.
	 */
	@Test
	public void ineligibleClassesOnStackNotCLIEntry() throws Throwable
	{
		class Ineligible implements Runnable
		{
			Object kick() throws Throwable
			{
				return EntrySub1.pass();
			}

			public void run()
			{
				try
				{
					Assert.assertTrue(CommonsCLILauncher.main(new String[]{}) instanceof EntrySub0);
				}
				catch (Exception e)
				{
					throw new IllegalArgumentException(e);
				}
			}
		}

		Assert.assertTrue(new Ineligible().kick() instanceof EntrySub0);
		EntrySub0.passThis(new Ineligible());
	}
}