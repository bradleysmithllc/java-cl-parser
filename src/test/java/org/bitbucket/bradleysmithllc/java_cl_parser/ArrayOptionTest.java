package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Assert;
import org.junit.Test;

@CLIEntry(
		nickName = "run",
		description = "I do lot's of things",
		version = "1.0"
)
public class ArrayOptionTest
{
	String [] vals;

	@CLIOption(
		name = "tc",
		longName = "thread-count",
		description = "",
		required = true,
		valueType = CLIOption.value_type.required,
		valueCardinality = CLIOption.UNLIMITED_VALUES,
		valueSeparator = '/'
	)
	public void setThreadCount(String [] vals)
	{
		this.vals = vals;
	}

	@CLIMain
	public void main()
	{
	}

	@Test
	public void testShortOptions() throws Throwable
	{
		CommonsCLILauncher.mainWithInstance(this, new String[]{"-tc", "10/20/30/40\\/50"});

		Assert.assertNotNull(vals);
		Assert.assertEquals(4, vals.length);
		Assert.assertEquals("10", vals[0]);
		Assert.assertEquals("20", vals[1]);
		Assert.assertEquals("30", vals[2]);
		Assert.assertEquals("40/50", vals[3]);
	}
}