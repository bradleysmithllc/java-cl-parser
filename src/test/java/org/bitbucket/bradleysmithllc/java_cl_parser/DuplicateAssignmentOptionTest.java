package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0",
		assignmentOrder = {"tc2", "tc"}
)
public class DuplicateAssignmentOptionTest
{
	@CLIOption(
			name = "tc2",
			longName = "tc"
	)
	public void setThreadCount2(int count)
	{
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void duplicatedName() throws Throwable
	{
		DuplicateAssignmentOptionTest cliLaunchable = new DuplicateAssignmentOptionTest();
		CommonsCLILauncher.mainWithInstance(cliLaunchable, new String[]{});
	}
}