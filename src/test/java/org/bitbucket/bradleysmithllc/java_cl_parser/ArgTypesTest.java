package org.bitbucket.bradleysmithllc.java_cl_parser;

import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.java_cl_parser.support.ArgTool;
import org.junit.Test;

public class ArgTypesTest
{
	@Test
	public void argSupplied() throws Throwable
	{
		ArgTool basicTest = new ArgTool(true, true, true);
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-tc", "-flag1", "-flag2"});
		Assert.assertTrue("Flags don't match expected state: " + basicTest.state(), basicTest.isSet());
	}

	@Test
	public void argNotSupplied() throws Throwable
	{
		ArgTool basicTest = new ArgTool(true, false, false);
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{});
		Assert.assertTrue("Flags don't match expected state: " + basicTest.state(), basicTest.isSet());
	}
}