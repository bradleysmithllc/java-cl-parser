package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

@CLIEntry(
		nickName = "run",
		description = "I do lot's of things",
		version = "1.0"
)
public class VerboseTest
{
	@CLIOption(
		name = "tc",
		longName = "thread-count",
		valueType = CLIOption.value_type.required,
		defaultValue = "1"
	)
	public void setThreadCount(int count)
	{
	}

	@CLIOption(
		name = "tc3",
		longName = "thread-count-3",
		valueType = CLIOption.value_type.required,
		defaultValue = "${thread-count}1"
	)
	public void setThreadCount3(int count)
	{
	}

	@CLIOption(
			name = "tc1",
			longName = "thread-count1",
			valueType = CLIOption.value_type.flag
	)
	public void setThreadCount2(int count)
	{
	}

	@CLIOption(
			name = "tc2",
			longName = "thread-count1-context-verified-and-sealed",
			valueType = CLIOption.value_type.flag
	)
	public void setThreadCoun1(int count)
	{
	}

	@CLIOption(
			name = "tc4",
			longName = "thread-count3-context-verified-and-sealed",
			valueCardinality = CLIOption.UNLIMITED_VALUES,
			defaultValue = "1,2,3,4,5,6"
	)
	public void setThreadCoun3(int [] count)
	{
	}

	@CLIMain
	public void main()
	{
	}

	@Test
	public void testShortOptions() throws Throwable
	{
		VerboseTest basicTest = new VerboseTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-v"});
	}

	@Test
	public void testLongOptions() throws Throwable
	{
		VerboseTest basicTest = new VerboseTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"--verbose"});
	}
}