package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

import java.io.IOException;

/**
 * This is separated out because maven doesn't pass the unicode test.
 */
public class LargeTextUnicode extends LargeTextTest
{
	@Test
	public void tale_of_two_cities() throws IOException
	{
		compare("tale_of_two_cities", 6535);
	}
}