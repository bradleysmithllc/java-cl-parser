package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Assert;
import org.junit.Test;

@CLIEntry(
	nickName = "run",
	description = "DEATH be not proud, though some have called thee Mighty and dreadfull, for, thou art not so, For, those, whom thou think'st, thou dost overthrow, Die not, poore death, nor yet canst thou kill me.  From rest and sleepe, which but thy pictures bee, Much pleasure, then from thee, much more must flow, And soonest our best men with thee doe goe, Rest of their bones, and soules deliverie.  Thou art slave to Fate, Chance, kings, and desperate men, And dost with poyson, warre, and sicknesse dwell, And poppie, or charmes can make us sleepe as well, And better then thy stroake; why swell'st thou then; One short sleepe past, wee wake eternally, And death shall be no more; death, thou shalt die.",
	contact = "bob@apple.com",
	documentationUrl = "http://wiki.com/help",
	versionControl = "svn:svc:http://wagada",
	version = "1.0"
)
public class HelpTest extends ShutdownMethodsBase
{
	@CLIOption(
		name = "opflag",
		valueType = CLIOption.value_type.flag,
		description = ""
	)
	public void set1(int count)
	{
	}

	@CLIOption(
			name = "reqflag",
			longName = "required-flag",
			required = true,
			valueType = CLIOption.value_type.flag,
			description = ""
	)
	public void set2(int count)
	{
	}

	@CLIOption(
			name = "optoptarg",
			longName = "optional-with-argument",
			description = ""
	)
	public void set3(int count)
	{
	}

	@CLIOption(
			name = "reqoptarg",
			required = true,
			longName = "required-with-argument",
			description = ""
	)
	public void set4(int count)
	{
	}

	@CLIOption(
			name = "optreqarg",
			longName = "optional-with-required-argument",
			valueType = CLIOption.value_type.required,
			description = ""
	)
	public void set5(int count)
	{
	}

	@CLIOption(
			name = "reqreqarg",
			required = true,
			longName = "required-with-required-argument",
			valueType = CLIOption.value_type.required,
			description = ""
	)
	public void set6(int count)
	{
	}

	@CLIOption(
			name = "optoptenum",
			longName = "optional-with-optional-enum",
			valueType = CLIOption.value_type.required,
			enumeratedValues = {"YES","NO"},
			description = ""
	)
	public void set7(int count)
	{
	}

	@CLIOption(
			name = "reqoptenum",
			required = true,
			longName = "required-with-optional-enum",
			valueType = CLIOption.value_type.required,
			enumeratedValues = {"YES","NO"},
			description = ""
	)
	public void set8(int count)
	{
	}

	@CLIOption(
			name = "optreqenum",
			longName = "optional-with-required-enum",
			valueType = CLIOption.value_type.required,
			enumeratedValues = {"YES","NO"},
			description = ""
	)
	public void set9(int count)
	{
	}

	@CLIOption(
			name = "reqreqenum",
			required = true,
			longName = "required-with-required-enum",
			valueType = CLIOption.value_type.required,
			enumeratedValues = {"YES","NO"},
			description = ""
	)
	public void set10(int count)
	{
	}

	@CLIOption(
			name = "arrayoptNarg",
			valueType = CLIOption.value_type.required,
			valueCardinality = 0,
			description = ""
	)
	public void set11(int [] count)
	{
	}

	@CLIOption(
			name = "arrayreqNarg",
			valueType = CLIOption.value_type.required,
			valueCardinality = CLIOption.UNLIMITED_VALUES,
			description = ""
	)
	public void set12(int [] count)
	{
	}

	@CLIOption(
			name = "arrayopt6arg",
			valueType = CLIOption.value_type.required,
			valueCardinality = 6,
			description = ""
	)
	public void set13(String [] count)
	{
	}

	@CLIOption(
			name = "arrayreq6arg",
			valueType = CLIOption.value_type.required,
			valueCardinality = 6,
			description = ""
	)
	public void set14(String [] count)
	{
	}

	@CLIOption(
			name = "arrayoptNargpipe",
			valueType = CLIOption.value_type.required,
			valueCardinality = CLIOption.UNLIMITED_VALUES,
			valueSeparator = '|',
			description = ""
	)
	public void set15(boolean [] count)
	{
	}

	@CLIOption(
			name = "arrayreqNargpipe",
			valueType = CLIOption.value_type.required,
			valueCardinality = CLIOption.UNLIMITED_VALUES,
			valueSeparator = '|',
			description = ""
	)
	public void set16(long [] count)
	{
	}

	@CLIOption(
			name = "arrayopt6argpipe",
			valueType = CLIOption.value_type.required,
			valueCardinality = 6,
			valueSeparator = '|',
			description = ""
	)
	public void set17(String [] count)
	{
	}

	@CLIOption(
			name = "arrayreq6argpipe",
			valueType = CLIOption.value_type.required,
			valueCardinality = 6,
			valueSeparator = '|',
			description = ""
	)
	public void set18(int [] count)
	{
	}

	@CLIOption(
			name = "arrayoptNargenum",
			valueType = CLIOption.value_type.required,
			valueCardinality = CLIOption.UNLIMITED_VALUES,
			enumeratedValues = {"Y", "N"},
			description = ""
	)
	public void set19(String [] count)
	{
	}

	@CLIOption(
			name = "arrayreqNargenum",
			valueType = CLIOption.value_type.required,
			valueCardinality = CLIOption.UNLIMITED_VALUES,
			enumeratedValues = {"Y", "N"},
			description = ""
	)
	public void set20(String [] count)
	{
	}

	@CLIOption(
			name = "arraywithdefault",
			valueType = CLIOption.value_type.required,
			defaultValue = "12",
			valueCardinality = CLIOption.UNLIMITED_VALUES,
			description = ""
	)
	public void set21(String [] count)
	{
	}

	@CLIOption(
			name = "arraywithdefaultamp",
			valueType = CLIOption.value_type.required,
			defaultValue = "12,3,2&5.4,3,2|",
			valueCardinality = CLIOption.UNLIMITED_VALUES,
			valueSeparator = '&',
			description = ""
	)
	public void set22(String [] count)
	{
	}

	@CLIMain
	public void main()
	{
	}

	@Test(expected = UsageException.class)
	public void testShortOptions() throws Throwable
	{
		HelpTest basicTest = new HelpTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-?"});
		assertExitCode(0);
	}

	@Test(expected = UsageException.class)
	public void testLongOptions() throws Throwable
	{
		HelpTest basicTest = new HelpTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"--help"});
		assertExitCode(0);
	}

	@Test
	public void testShortOptionsClean() throws Throwable
	{
		HelpTest basicTest = new HelpTest();
		CommonsCLILauncher.mainWithInstanceClean(basicTest, new String[]{"-?"});
		assertExitCode(0);
	}

	@Test
	public void testLongOptionsClean() throws Throwable
	{
		HelpTest basicTest = new HelpTest();
		CommonsCLILauncher.mainWithInstanceClean(basicTest, new String[]{"--help"});
		assertExitCode(0);
	}

	@Test
	public void helpWithOptions() throws Throwable
	{
		try
		{
			HelpTest basicTest = new HelpTest();
			CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-?", "-tcl"});
		}
		catch(UsageException exc)
		{
			Assert.assertEquals(CommonsCLILauncher.HELP_INVALID_USAGE_MESSAGE, exc.getMessage());
		}
	}

	@Test
	public void helpLongWithoptions() throws Throwable
	{
		try
		{
			HelpTest basicTest = new HelpTest();
			CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"--help", "-tcl"});
		}
		catch(UsageException exc)
		{
			Assert.assertEquals(CommonsCLILauncher.HELP_INVALID_USAGE_MESSAGE, exc.getMessage());
		}
	}

	@Test
	public void helpWithOptionsClean() throws Throwable
	{
		HelpTest basicTest = new HelpTest();
		CommonsCLILauncher.mainWithInstanceClean(basicTest, new String[]{"-?"});
		assertExitCode(0);
	}

	@Test
	public void helpLongWithoptionsClean() throws Throwable
	{
		HelpTest basicTest = new HelpTest();
		CommonsCLILauncher.mainWithInstanceClean(basicTest, new String[]{"--help", "-tc"});
		assertExitCode(-1);
	}
}