package org.bitbucket.bradleysmithllc.java_cl_parser.regexp.test;

import org.bitbucket.bradleysmithllc.java_cl_parser.regexp.EscapeFunctionExpression;
import org.junit.Assert;
import org.junit.Test;

public class EscapeFunctionTest
{
	@Test
	public void matches()
	{
		Assert.assertTrue(new EscapeFunctionExpression("\\{1}").matches());
		Assert.assertTrue(new EscapeFunctionExpression("\\{      1        }").matches());

		Assert.assertFalse(new EscapeFunctionExpression("\\ {1}").matches());
		Assert.assertFalse(new EscapeFunctionExpression("\\{1} ").matches());


		Assert.assertTrue(new EscapeFunctionExpression("\\\\{1}").matches());
		Assert.assertFalse(new EscapeFunctionExpression("\\{1").matches());
		Assert.assertFalse(new EscapeFunctionExpression("\\1}").matches());
		Assert.assertFalse(new EscapeFunctionExpression("\\{}").matches());
	}

	@Test
	public void find()
	{
		Assert.assertTrue(new EscapeFunctionExpression("\\ \\{1} ").hasNext());
		Assert.assertTrue(new EscapeFunctionExpression("\\\\{1} ").hasNext());

		EscapeFunctionExpression escapeFunctionExpression = new EscapeFunctionExpression("\\{1}");
		Assert.assertTrue(escapeFunctionExpression.hasNext());
		Assert.assertEquals("1", escapeFunctionExpression.getEscapeFunction());
		Assert.assertFalse(escapeFunctionExpression.hasNext());

		escapeFunctionExpression = new EscapeFunctionExpression("\\{  1  }");
		Assert.assertTrue(escapeFunctionExpression.hasNext());
		Assert.assertEquals("  1  ", escapeFunctionExpression.getEscapeFunction());
		Assert.assertFalse(escapeFunctionExpression.hasNext());

		escapeFunctionExpression = new EscapeFunctionExpression("\\{  \\\\\\\\ [] \\\\\\\\  }");
		Assert.assertTrue(escapeFunctionExpression.hasNext());
		Assert.assertEquals("  \\\\\\\\ [] \\\\\\\\  ", escapeFunctionExpression.getEscapeFunction());
		Assert.assertEquals("\\", escapeFunctionExpression.getPreChar());
		Assert.assertTrue(escapeFunctionExpression.hasPreChar());
		Assert.assertFalse(escapeFunctionExpression.hasNext());

		escapeFunctionExpression = new EscapeFunctionExpression("  \\\\{Blah}\\{blah}\\\\{Blah}  ");
		Assert.assertTrue(escapeFunctionExpression.hasNext());
		Assert.assertEquals("\\\\", escapeFunctionExpression.getPreChar());
		Assert.assertEquals("Blah", escapeFunctionExpression.getEscapeFunction());
		Assert.assertTrue(escapeFunctionExpression.hasPreChar());

		Assert.assertTrue(escapeFunctionExpression.hasNext());
		Assert.assertEquals("blah", escapeFunctionExpression.getEscapeFunction());
		Assert.assertEquals("\\", escapeFunctionExpression.getPreChar());

		Assert.assertTrue(escapeFunctionExpression.hasNext());
		Assert.assertEquals("Blah", escapeFunctionExpression.getEscapeFunction());
		Assert.assertEquals("\\\\", escapeFunctionExpression.getPreChar());

		Assert.assertFalse(escapeFunctionExpression.hasNext());

		escapeFunctionExpression = new EscapeFunctionExpression("\\{\\\\s\\e}");
		Assert.assertTrue(escapeFunctionExpression.hasNext());
		Assert.assertEquals("\\", escapeFunctionExpression.getPreChar());
		Assert.assertEquals("\\\\s\\e", escapeFunctionExpression.getEscapeFunction());
		Assert.assertTrue(escapeFunctionExpression.hasPreChar());
	}
}