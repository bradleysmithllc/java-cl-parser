package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class DuplicatedLongOptionsTest
{
	@CLIOption(
			name = "tc1",
			longName = "tc"
	)
	public void setThreadCount1(int count)
	{
	}

	@CLIOption(
			name = "tc2",
			longName = "tc"
	)
	public void setThreadCount2(int count)
	{
	}

	public void main()
	{
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void duplicatedName() throws Throwable
	{
		DuplicatedLongOptionsTest cliLaunchable = new DuplicatedLongOptionsTest();
		CommonsCLILauncher.mainWithInstance(cliLaunchable, new String[]{});
	}
}