package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.bitbucket.bradleysmithllc.java_cl_parser.support.*;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

public class OptionSetTest
{
	@Test(expected = UsageException.class)
	public void testNoOptions() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValid(), null);
	}

	@Test
	public void testValidSet1() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValid(), new String [] {"-opt1"});
	}

	@Test
	public void testValidSet2() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValid(), new String [] {"-opt1", "-opt2"});
	}

	@Test
	public void testValidSet3() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValid(), new String [] {"-opt1", "-opt2", "-opt3"});
	}

	@Test
	public void testValidSet4() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValid(), new String [] {"-opt1", "-opt2", "-opt3", "-opt4"});
	}

	@Test(expected = UsageException.class)
	public void testInvalidSet1() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValid(), new String [] {"-opt1", "-opt2", "-opt4"});
	}

	@Test(expected = UsageException.class)
	public void testInvalidSet2() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValid(), new String [] {"-opt1", "-opt4"});
	}

	@Test(expected = UsageException.class)
	public void testInvalidSet3() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValid(), new String [] {"-opt4"});
	}

	@Test
	public void testNoOptionsInv() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalid(), null);
	}

	@Test(expected = UsageException.class)
	public void testValidSet1Inv() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalid(), new String [] {"-opt1"});
	}

	@Test(expected = UsageException.class)
	public void testValidSet2Inv() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalid(), new String [] {"-opt1", "-opt2"});
	}

	@Test(expected = UsageException.class)
	public void testValidSet3Inv() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalid(), new String [] {"-opt1", "-opt2", "-opt3"});
	}

	@Test(expected = UsageException.class)
	public void testValidSet4Inv() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalid(), new String [] {"-opt1", "-opt2", "-opt3", "-opt4"});
	}

	@Test
	public void testInvalidSet1Inv() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalid(), new String [] {"-opt1", "-opt2", "-opt4"});
	}

	@Test
	public void testInvalidSet2Inv() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalid(), new String [] {"-opt1", "-opt4"});
	}

	@Test
	public void testInvalidSet3Inv() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalid(), new String [] {"-opt4"});
	}

	@Test
	public void testTypes1() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValidTypes(), new String [] {"-opt1", "-opt2"});
	}

	@Test
	public void testTypes2() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValidTypes(), new String [] {"-opt1", "-opt2", "-opt3"});
	}

	@Test
	public void testTypes3() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValidTypes(), new String [] {"-opt1", "-opt2", "-opt3", "-opt4"});
	}

	@Test(expected = UsageException.class)
	public void testTypes4() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValidTypes(), new String [] {"-opt1", "-opt3", "-opt4"});
	}

	@Test(expected = UsageException.class)
	public void testTypes5() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsValidTypes(), new String [] {"-opt2", "-opt3", "-opt4"});
	}

	@Test(expected = UsageException.class)
	public void testTypes6() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalidTypes(), new String [] {"-opt1", "-opt2"});
	}

	@Test(expected = UsageException.class)
	public void testTypes7() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalidTypes(), new String [] {"-opt1", "-opt2", "-opt3"});
	}

	@Test(expected = UsageException.class)
	public void testTypes8() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalidTypes(), new String [] {"-opt1", "-opt2", "-opt3", "-opt4"});
	}

	@Test
	public void testTypes9() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalidTypes(), new String [] {"-opt1", "-opt3", "-opt4"});
	}

	@Test
	public void testTypes10() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsInvalidTypes(), new String [] {"-opt2", "-opt3", "-opt4"});
	}

	@Test(expected = UsageException.class)
	public void testTypes11() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsBoth(), new String [] {"-opt2", "-opt3", "-opt4"});
	}

	@Test(expected = UsageException.class)
	public void testTypes12() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsBoth(), new String [] {"-opt1", "-opt2", "-opt3", "-opt4"});
	}

	@Test
	public void testTypes13() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new CLIOptionSetsBoth(), new String [] {"-opt1", "-opt2", "-opt4"});
	}
}