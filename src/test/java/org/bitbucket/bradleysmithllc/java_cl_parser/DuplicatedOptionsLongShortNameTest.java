package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class DuplicatedOptionsLongShortNameTest
{
	@CLIOption(
			name = "tc"
	)
	public void setThreadCount(int count)
	{
	}

	@CLIOption(
			name = "tc2",
			longName = "tc"
	)
	public void setThreadCountDup(int count)
	{
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void duplicatedName() throws Throwable
	{
		DuplicatedOptionsLongShortNameTest cliLaunchable = new DuplicatedOptionsLongShortNameTest();
		CommonsCLILauncher.mainWithInstance(cliLaunchable, new String[]{});
	}
}