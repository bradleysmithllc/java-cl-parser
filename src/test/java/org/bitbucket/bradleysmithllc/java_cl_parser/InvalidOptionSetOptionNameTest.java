package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.bitbucket.bradleysmithllc.java_cl_parser.support.*;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

public class InvalidOptionSetOptionNameTest
{
	@Test(expected = InvalidCLIEntryException.class)
	public void testValid() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new InvalidCLIOptionsMissingOption(), null);
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void testInvalid() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new InvalidCLIOptionsMissingOptionInvalid(), null);
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void testOverlapping() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new InvalidCLIOptionsOverlappingSets(), null);
	}

	/**
	 * This is no longer invalid
	 * @throws InvalidCLIEntryException
	 * @throws UsageException
	 */
	//@Test(expected = InvalidCLIEntryException.class)
	public void testOverlappingInvalid() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new InvalidCLIOptionsOverlappingSetsInvalid(), null);
	}

	/**
	 * This is no longer invalid
	 * @throws InvalidCLIEntryException
	 * @throws UsageException
	 */
	//@Test(expected = InvalidCLIEntryException.class)
	public void testOverlappingValid() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new InvalidCLIOptionsOverlappingSetsValid(), null);
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void testDuplicateValid() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new InvalidCLIOptionsDuplicateValid(), null);
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void testDuplicateInvalid() throws InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.mainWithInstance(new InvalidCLIOptionsDuplicateInvalid(), null);
	}
}