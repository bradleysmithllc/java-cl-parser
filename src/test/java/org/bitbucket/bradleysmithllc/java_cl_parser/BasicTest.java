package org.bitbucket.bradleysmithllc.java_cl_parser;

import junit.framework.Assert;
import org.junit.Test;

@CLIEntry(
		nickName = "run",
		description = "I do lot's of things",
		version = "1.0"
)
public class BasicTest
{
	private boolean mained = false;
	private int threadCount = 0;
	private boolean threadCountSet = false;

	@CLIOption(
				name = "tc",
				longName = "thread-count",
				description = "",
				required = true,
				valueType = CLIOption.value_type.required
		)
	public void setThreadCount(int count)
	{
		threadCount = count;
		threadCountSet = true;
	}

	@CLIMain
	public void main()
	{
		mained = true;
	}

	@Test
	public void testShortOptions() throws Throwable
	{
		BasicTest basicTest = new BasicTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-tc", "10"});

		Assert.assertTrue("Main method not invoked", basicTest.mained);
		Assert.assertTrue("Thread count setter not called", basicTest.threadCountSet);
		Assert.assertEquals(basicTest.threadCount, 10);
	}

	@Test
	public void testLongOptions() throws Throwable
	{
		BasicTest basicTest = new BasicTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"--thread-count", "11"});

		Assert.assertTrue("Main method not invoked", basicTest.mained);
		Assert.assertTrue("Thread count setter not called", basicTest.threadCountSet);
		Assert.assertEquals(basicTest.threadCount, 11);
	}
}