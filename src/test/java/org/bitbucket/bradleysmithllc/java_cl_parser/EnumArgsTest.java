package org.bitbucket.bradleysmithllc.java_cl_parser;

import junit.framework.Assert;
import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class EnumArgsTest
{
	private String val;

	@CLIOption(
			name = "tc",
			valueType = CLIOption.value_type.required,
			enumeratedValues = {"yes", "no"}
	)
	public void setThreadCount(String ival)
	{
		Assert.assertEquals(val, ival);
	}

	public void main()
	{
	}

	@Test
	public void argSupplied() throws Throwable
	{
		 val = "yes";
		CommonsCLILauncher.mainWithInstance(this, new String[]{"-tc", "yes"});

		val = "no";
		CommonsCLILauncher.mainWithInstance(this, new String[]{"-tc", "no"});
	}

	@Test(expected = UsageException.class)
	public void argNotInRange() throws Throwable
	{
		val = "fail";
		CommonsCLILauncher.mainWithInstance(this, new String[]{"-tc", "false"});
	}
}