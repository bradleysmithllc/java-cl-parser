package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

@CLIEntry(
		nickName = "run",
		description = "I do lot's of things",
		version = "1.0"
)
public class ContextBadParameterTypeTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@CLIOption(
				name = "tc",
				longName = "thread-count",
				description = "",
				required = true,
				valueType = CLIOption.value_type.required
		)
	public void setThreadCount(int count)
	{
	}

	@CLIContext
	public void context(int count)
	{
	}

	@CLIMain
	public void main()
	{
	}

	@Test
	public void testLongOptions() throws Throwable
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Context method must receive one parameter of type CLParserContext... public void org.bitbucket.bradleysmithllc.java_cl_parser.ContextBadParameterTypeTest.context(int)");

		ContextBadParameterTypeTest basicTest = new ContextBadParameterTypeTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"--thread-count", "11"});
	}
}