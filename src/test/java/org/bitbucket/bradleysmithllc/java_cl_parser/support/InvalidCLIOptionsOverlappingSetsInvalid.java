package org.bitbucket.bradleysmithllc.java_cl_parser.support;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOptionSet;

@CLIEntry(
	description = "",
	version = "1.0",
	validarameterSets = {
			@CLIOptionSet(id = "1", optionShortNames = {"opt1"}, description = "Stuff v")
	},
	invalidarameterSets = {
			@CLIOptionSet(id = "1", optionShortNames = {"opt1", "opt2"}, description = "Stuff inv")
	}
)
public class InvalidCLIOptionsOverlappingSetsInvalid
{
	@CLIOption(
		name = "opt1"
	)
	public void set(boolean val){}

	@CLIOption(
		name = "opt2"
	)
	public void set2(boolean val){}

	@CLIMain
	public void v(){}
}