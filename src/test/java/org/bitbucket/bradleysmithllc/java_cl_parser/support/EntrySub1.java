package org.bitbucket.bradleysmithllc.java_cl_parser.support;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.junit.Assert;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class EntrySub1
{
	@CLIMain
	public void main()
	{
		Assert.fail();
	}

	public static Object pass() throws Throwable
	{
		return EntrySub0.pass();
	}

	public static void passThis(Runnable runner)
	{
		EntrySub0.passThis(runner);
	}
}