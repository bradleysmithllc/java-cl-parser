package org.bitbucket.bradleysmithllc.java_cl_parser.support;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOptionSet;

@CLIEntry(
		description = "",
		version = "1.0",
		validarameterSets = {
				@CLIOptionSet(id = "2", optionShortNames = {"opt1", "opt2"}, description = "Stuff v", setType = CLIOptionSet.set_type.subset_of_args)
		}
)
public class CLIOptionSetsValidTypes {
	@CLIOption(
			name = "opt1",
			valueType = CLIOption.value_type.flag
	)
	public void set(boolean val) {
	}

	@CLIOption(
			name = "opt2",
			valueType = CLIOption.value_type.flag
	)
	public void set2(boolean val) {
	}

	@CLIOption(
			name = "opt3",
			valueType = CLIOption.value_type.flag
	)
	public void set3(boolean val) {
	}

	@CLIOption(
			name = "opt4",
			valueType = CLIOption.value_type.flag
	)
	public void set4(boolean val) {
	}

	@CLIMain
	public void v() {
	}
}