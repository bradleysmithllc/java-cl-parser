package org.bitbucket.bradleysmithllc.java_cl_parser.support;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOptionSet;

@CLIEntry(
	description = "",
	version = "1.0",
	validarameterSets = {
			@CLIOptionSet(id = "1", optionShortNames = {"opt"}, description = "Stuff")
	},
	invalidarameterSets = {
			@CLIOptionSet(id = "1", optionShortNames = {"opt"}, description = "Stuff")
	}
)
public class InvalidCLIOptionsOverlappingSets
{
	@CLIOption(
		name = "opt"
	)
	public void set(boolean val){}

	@CLIMain
	public void v(){}
}