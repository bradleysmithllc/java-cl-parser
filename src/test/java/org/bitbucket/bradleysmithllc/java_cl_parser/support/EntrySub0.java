package org.bitbucket.bradleysmithllc.java_cl_parser.support;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class EntrySub0
{
	@CLIMain
	public void main()
	{
	}

	public static Object pass() throws Throwable
	{
		return CommonsCLILauncher.main(new String[]{});
	}

	public static void passThis(Runnable runner)
	{
		runner.run();
	}
}