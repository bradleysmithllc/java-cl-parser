package org.bitbucket.bradleysmithllc.java_cl_parser.support;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

@CLIEntry(
	description = "",
		version = "1.0"
)
public class ArgTool
{
	private final boolean flag1;
	private final boolean flag2;
	private final boolean tc;

	private boolean flag1_set;
	private boolean flag2_set;
	private boolean tc_set;

	public ArgTool(boolean flag1, boolean flag2, boolean tc)
	{
		this.flag1 = flag1;
		this.flag2 = flag2;
		this.tc = tc;

		// initialize the set variables to the inverse of what we expect
		flag1_set = !flag1;
		flag2_set = !flag2;
		tc_set = !tc;
	}

	@CLIOption(
			name = "flag1",
			valueType = CLIOption.value_type.flag,
			defaultValue = "true"
	)
	public void set1(boolean enabled)
	{
		flag1_set = enabled;
	}

	@CLIOption(
			name = "flag2",
			valueType = CLIOption.value_type.flag
	)
	public void set2(boolean enabled)
	{
		flag2_set = enabled;
	}

	@CLIOption(
			name = "tc",
			valueType = CLIOption.value_type.flag
	)
	public void setThreadCount(boolean enabled)
	{
		tc_set = enabled;
	}

	public boolean isFlag1_set()
	{
		return flag1_set;
	}

	public boolean isFlag2_set()
	{
		return flag2_set;
	}

	public boolean isTc_set()
	{
		return tc_set;
	}

	public boolean isSet()
	{
		return flag1_set == flag1 && flag2_set == flag2 && tc_set == tc;
	}

	public String state()
	{
		return new StringBuilder("flag1: ").append(flag1)
				.append(" | ").append("flag1_set: ").append(flag1_set)
				.append(" | ").append("flag2: ").append(flag2)
				.append(" | ").append("flag2_set: ").append(flag2_set)
				.append(" | ").append("tc: ").append(tc)
				.append(" | ").append("tc_set: ").append(tc_set).toString();
	}
}