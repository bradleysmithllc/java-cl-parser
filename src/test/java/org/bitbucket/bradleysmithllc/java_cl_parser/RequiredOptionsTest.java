package org.bitbucket.bradleysmithllc.java_cl_parser;

import junit.framework.Assert;
import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class RequiredOptionsTest
{
	private boolean mained = false;
	private int threadCount = 0;
	private boolean threadCountSet = false;
	private boolean countThreadSet = false;

	@CLIOption(
			name = "tc",
			required = true,
			valueType = CLIOption.value_type.required
	)
	public void setThreadCount(int count)
	{
		threadCount = count;
		threadCountSet = true;
	}

	@CLIOption(
			name = "ct",
			valueType = CLIOption.value_type.required
	)
	public void setCountThread(int count)
	{
		countThreadSet = true;
	}

	public void main()
	{
		mained = true;
	}

	@Test(expected = UsageException.class)
	public void requiredMissing() throws Throwable
	{
		RequiredOptionsTest basicTest = new RequiredOptionsTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{});
		Assert.assertTrue(basicTest.threadCountSet);
		Assert.assertFalse(basicTest.countThreadSet);
	}

	@Test
	public void requiredSupplied() throws Throwable
	{
		RequiredOptionsTest basicTest = new RequiredOptionsTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-tc", "1"});
		Assert.assertTrue(basicTest.threadCountSet);
		Assert.assertFalse(basicTest.countThreadSet);
	}

	@Test
	public void requiredOptionalSupplied() throws Throwable
	{
		RequiredOptionsTest basicTest = new RequiredOptionsTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-tc", "1", "-ct", "17"});
		Assert.assertTrue(basicTest.threadCountSet);
		Assert.assertTrue(basicTest.countThreadSet);
	}
}