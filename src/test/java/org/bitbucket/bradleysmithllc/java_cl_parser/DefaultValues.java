package org.bitbucket.bradleysmithllc.java_cl_parser;

import junit.framework.Assert;
import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class DefaultValues
{
	int tc = 0;

	@CLIOption(
			name = "tc",
			defaultValue = "10"
	)
	public void setThreadCount(int count)
	{
		tc = count;
		Assert.assertEquals(tc, 10);
	}

	boolean b;

	@CLIOption(
			name = "b",
			defaultValue = "true"
	)
	public void b(boolean vb)
	{
		b = vb;
	}

	public void main()
	{
	}

	@Test
	public void testBoolean() throws Throwable
	{
		DefaultValues basicTest = new DefaultValues();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{});

		Assert.assertEquals(basicTest.b, true);
	}

	@Test
	public void testBooleanWithOverride() throws Throwable
	{
		DefaultValues basicTest = new DefaultValues();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-b", "false"});

		Assert.assertEquals(basicTest.b, false);
	}

	@Test
	public void testShortOptions() throws Throwable
	{
		DefaultValues basicTest = new DefaultValues();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{});

		Assert.assertEquals(basicTest.tc, 10);

		CommonsCLILauncher.mainWithInstance(basicTest, null);
	}

	@Test
	public void testShortOptionsNullArgs() throws Throwable
	{
		DefaultValues basicTest = new DefaultValues();
		CommonsCLILauncher.mainWithInstance(basicTest, null);

		Assert.assertEquals(basicTest.tc, 10);
	}
}