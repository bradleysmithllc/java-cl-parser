package org.bitbucket.bradleysmithllc.java_cl_parser;

import junit.framework.Assert;
import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class ValueTypesAndSeparatorsTest
{
	private int meths = 0;

	@CLIOption(
			name = "tc_ia",
			valueCardinality = 2,
			valueSeparator = '|'
	)
	public void setThreadCount(int[] count)
	{
		meths |= 1;

		Assert.assertEquals(count.length, 2);
		Assert.assertEquals(count[0], 10);
		Assert.assertEquals(count[1], 2);
	}

	@CLIOption(
			name = "tc_iia",
			valueCardinality = 3,
			valueSeparator = ','
	)
	public void setThreadCount(Integer[] count)
	{
		meths |= 2;

		Assert.assertEquals(count.length, 3);
		Assert.assertEquals(count[0].intValue(), 1);
		Assert.assertEquals(count[1].intValue(), 20);
		Assert.assertEquals(count[2].intValue(), 30);
	}

	@CLIOption(
			name = "tc_ba",
			valueCardinality = 4,
			valueSeparator = ':'
	)
	public void setThreadCount(boolean[] count)
	{
		meths |= 4;

		Assert.assertEquals(count.length, 4);
		Assert.assertTrue(count[0]);
		Assert.assertFalse(count[1]);
		Assert.assertTrue(count[2]);
		Assert.assertFalse(count[3]);
	}

	@CLIOption(
			name = "tc_bba",
			valueCardinality = 5,
			valueSeparator = ';'
	)
	public void setThreadCount(Boolean[] count)
	{
		meths |= 8;

		Assert.assertEquals(count.length, 5);
		Assert.assertFalse(count[0].booleanValue());
		Assert.assertTrue(count[1].booleanValue());
		Assert.assertTrue(count[2].booleanValue());
		Assert.assertFalse(count[3].booleanValue());
		Assert.assertFalse(count[4].booleanValue());
	}

	public void main()
	{
	}

	@Test
	public void testShortOptions() throws Throwable
	{
		ValueTypesAndSeparatorsTest basicTest = new ValueTypesAndSeparatorsTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{
				"-tc_ia",
				"10|2",
				"-tc_iia",
				"1,20,30",
				"-tc_ba",
				"true:false:true:false",
				"-tc_bba",
				"false;true;true;false;false"
		});

		Assert.assertEquals(basicTest.meths, 15);
	}

	@Test(expected = UsageException.class)
	public void tooManyValues() throws Throwable
	{
		ValueTypesAndSeparatorsTest basicTest = new ValueTypesAndSeparatorsTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{
				"-tc_ia",
				"10|2|1|2"
		});

		Assert.assertEquals(basicTest.meths, 15);
	}

	@Test(expected = UsageException.class)
	public void tooFewValues() throws Throwable
	{
		ValueTypesAndSeparatorsTest basicTest = new ValueTypesAndSeparatorsTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{
				"-tc_ia",
				"10"
		});

		Assert.assertEquals(basicTest.meths, 15);
	}
}