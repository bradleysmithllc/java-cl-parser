package org.bitbucket.bradleysmithllc.java_cl_parser;

import junit.framework.Assert;
import org.junit.Test;

@CLIEntry(
		nickName = "run",
		description = "I do lot's of things",
		version = "1.0"
)
public class RecursiveTest
{
	@CLIOption(
		name = "tc",
		longName = "thread-count",
		description = "",
		valueType = CLIOption.value_type.required,
		defaultValue = "5"
	)
	public void setThreadCount(int count)
	{
	}

	@CLIOption(
		name = "tcs",
		longName = "thread-count-string",
		description = "",
		valueType = CLIOption.value_type.required,
		defaultValue = "1-${thread-count}-str"
	)
	public void setThreadCountStr(String counter)
	{
		Assert.assertEquals("1-6-str", counter);
	}

	@CLIMain
	public void main()
	{
	}

	@Test
	public void testShortOptions() throws Throwable
	{
		RecursiveTest basicTest = new RecursiveTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"-tc", "6"});
	}
}