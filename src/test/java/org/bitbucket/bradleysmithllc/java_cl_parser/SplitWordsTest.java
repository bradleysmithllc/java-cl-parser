package org.bitbucket.bradleysmithllc.java_cl_parser;

import junit.framework.Assert;
import org.junit.Test;

import java.util.List;

public class SplitWordsTest
{
	@Test
	public void normal()
	{
		List<String> words = StringUtil.splitWords("Hi");

		Assert.assertEquals(1, words.size());
		Assert.assertEquals("Hi", words.get(0));
	}

	@Test
	public void newLinesArePreserved()
	{
		List<String> words = StringUtil.splitWords("Hi\nthere");

		Assert.assertEquals(3, words.size());
		Assert.assertEquals("Hi", words.get(0));
		Assert.assertEquals("\n", words.get(1));
		Assert.assertEquals("there", words.get(2));
	}

	@Test
	public void whitespaceGetsCollapsed()
	{
		List<String> words = StringUtil.splitWords("           Hello     \r\t     Mom          ");

		Assert.assertEquals(2, words.size());
		Assert.assertEquals("Hello", words.get(0));
		Assert.assertEquals("Mom", words.get(1));
	}

	@Test
	public void nothingButWhitespace()
	{
		List<String> words = StringUtil.splitWords("                \r\t               ");

		Assert.assertEquals(0, words.size());
	}

	@Test
	public void punctuationGoesWithAttachedWord()
	{
		List<String> words = StringUtil.splitWords("Hello, Mom. \t.\r\"Hello\"");

		Assert.assertEquals(4, words.size());
		Assert.assertEquals("Hello,", words.get(0));
		Assert.assertEquals("Mom.", words.get(1));
		Assert.assertEquals(".", words.get(2));
		Assert.assertEquals("\"Hello\"", words.get(3));
	}

	@Test
	public void punctuation()
	{
		String text = "\"':;,.?/\\!@#$%^&*()_+-=~[]{}\\|,.<>/?";
		List<String> words = StringUtil.splitWords(text);

		Assert.assertEquals(1, words.size());
		Assert.assertEquals(text, words.get(0));
	}
}