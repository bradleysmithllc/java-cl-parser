package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

@CLIEntry(
		nickName = "run",
		description = "I do lot's of things",
		version = "1.0"
)
public class CleanMethodsTest extends ShutdownMethodsBase {

	@CLIOption(
			name = "tc",
			longName = "thread-count",
			required = true
	)
	public void setThreadCount(int count)
	{
	}

	@CLIMain
	public void main()
	{
	}

	@Test(expected = UsageException.class)
	public void rough() throws Throwable
	{
		ShutdownMethodsBase basicTest = new CleanMethodsTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{});
		assertExitCode(-1);
	}

	@Test
	public void clean() throws Throwable
	{
		ShutdownMethodsBase basicTest = new CleanMethodsTest();
		CommonsCLILauncher.mainWithInstanceClean(basicTest, new String[]{});
		assertExitCode(-1);
	}

	@Test(expected = UsageException.class)
	public void roughStack() throws Throwable
	{
		CommonsCLILauncher.main(new String[]{});
		assertExitCode(-1);
	}

	@Test
	public void cleanStack() throws Throwable
	{
		CommonsCLILauncher.mainClean(new String[]{});
		assertExitCode(-1);
	}
}