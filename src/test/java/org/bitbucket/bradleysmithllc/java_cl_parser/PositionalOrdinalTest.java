package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.reflect.Executable;

@CLIEntry(
		description = "",
		version = "1.0",
		assignmentOrder = {"tc2", "tc3"}
)
public class PositionalOrdinalTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	private int tc2 = -1;
	private int tc3 = -1;
	private int tc4 = -1;

	@CLIOption(
		name = "tc2",
		longName = "tc2",
		unnamedArgumentOrdinal = 1
	)
	public void setThreadCount2(int count)
	{
		tc2 = count;
	}

	@CLIOption(
			name = "tc3",
			longName = "tc3",
			unnamedArgumentOrdinal = 2
	)
	public void setThreadCount3(int count)
	{
		tc3 = count;
	}

	@CLIOption(
			name = "tc4",
			longName = "tc4",
			unnamedArgumentOrdinal = 3
	)
	public void setThreadCount4(int count)
	{
		tc4 = count;
	}

	@Test
	public void ordinals() throws Throwable
	{
		PositionalOrdinalTest pot = new PositionalOrdinalTest();
		CommonsCLILauncher.mainWithInstance(pot, new String[]{"1", "2"});

		Assert.assertEquals(1, pot.tc2);
		Assert.assertEquals(2, pot.tc3);
	}

	@Test
	public void ordinalAndFlaginal() throws Throwable
	{
		PositionalOrdinalTest pot = new PositionalOrdinalTest();
		CommonsCLILauncher.mainWithInstance(pot, new String[]{"1", "-tc4", "3", "2"});

		Assert.assertEquals(1, pot.tc2);
		Assert.assertEquals(2, pot.tc3);
		Assert.assertEquals(3, pot.tc4);
	}

	@Test
	public void dupOrdinalAndFlaginal() throws Throwable
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Option [tc2] already specified as a flag");

		PositionalOrdinalTest pot = new PositionalOrdinalTest();
		CommonsCLILauncher.mainWithInstance(pot, new String[]{"-tc2", "3", "1"});
	}

	@Test
	public void dontRepeat() throws Throwable
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Option [tc2] already specified as a flag");

		PositionalOrdinalTest pot = new PositionalOrdinalTest();
		CommonsCLILauncher.mainWithInstance(pot, new String[]{"-tc2", "3", "-tc2", "4"});
	}
}