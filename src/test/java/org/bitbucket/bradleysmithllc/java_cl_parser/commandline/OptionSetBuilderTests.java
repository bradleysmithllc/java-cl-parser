package org.bitbucket.bradleysmithllc.java_cl_parser.commandline;

import org.junit.Assert;
import org.junit.Test;

public class OptionSetBuilderTests {
	@Test
	public void builder() {
		OptionSet os = new OptionSetBuilder().option("long").shortName("short").build();

		Assert.assertEquals(1, os.options().size());
	}

	@Test
	public void twoOptions() {
		OptionSet os = new OptionSetBuilder().option("long").shortName("short").option("name2").build();

		Assert.assertEquals(2, os.options().size());
	}

	@Test
	public void nameIsLongAndShortName() {
		OptionSet os = new OptionSetBuilder().option("names").build();

		Assert.assertEquals(1, os.options().size());
		Assert.assertEquals("names", os.options().get(0).shortName());
		Assert.assertEquals("names", os.options().get(0).longName());
	}

	@Test
	public void all() {
		OptionSet os = new OptionSetBuilder()
				.option("ln1").shortName("sn1").flag(true).optional(true)
				.option("ln2").shortName("sn2").flag(false).optional(false)
				.build();

		Assert.assertEquals(2, os.options().size());

		Assert.assertEquals("sn1", os.options().get(0).shortName());
		Assert.assertEquals("ln1", os.options().get(0).longName());
		Assert.assertTrue(os.options().get(0).flag());
		Assert.assertTrue(os.options().get(0).optional());

		Assert.assertEquals("sn2", os.options().get(1).shortName());
		Assert.assertEquals("ln2", os.options().get(1).longName());
		Assert.assertFalse(os.options().get(1).flag());
		Assert.assertFalse(os.options().get(1).optional());
	}
}