package org.bitbucket.bradleysmithllc.java_cl_parser.commandline;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CommandLineTests {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void none() throws ParseException {
		new CommandLine(new OptionSetBuilder().build(), "");
	}

	@Test
	public void oneFlag() throws ParseException {
		CommandLine cl = new CommandLine(new OptionSetBuilder().option("flg").flag(true).build(), "-flg");

		Assert.assertTrue(cl.optionSpecified("flg"));
		Assert.assertEquals("true", cl.getOptionValue("flg"));
	}

	@Test
	public void danglingOption() throws ParseException {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Option dangling wanting value 'opt'");

		new CommandLine(new OptionSetBuilder().option("opt").build(), "-opt");
	}

	@Test
	public void shortNamesAreDash() throws ParseException {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Option not defined '-opt'");

		new CommandLine(new OptionSetBuilder().option("opt").shortName("shrt").build(), "-opt", "val");
	}

	@Test
	public void longNamesAreDashDash() throws ParseException {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Option not defined '--shrt'");

		new CommandLine(new OptionSetBuilder().option("opt").shortName("shrt").build(), "--shrt", "val");
	}

	@Test
	public void mix() throws ParseException {
		CommandLine cl = new CommandLine(new OptionSetBuilder()
				.option("long-flag")
				.shortName("shrt-flag")
				.flag(true)
				.option("long-not-flag")
				.shortName("shrt-not-flag")
				.build(), "--long-flag", "-shrt-not-flag", "text");

		Assert.assertTrue(cl.optionSpecified("long-flag"));
		Assert.assertTrue(cl.optionSpecified("shrt-flag"));
		Assert.assertTrue(cl.optionSpecified("long-not-flag"));
		Assert.assertTrue(cl.optionSpecified("shrt-not-flag"));
	}

	@Test
	public void flagDefaultTrue() throws ParseException {
		CommandLine cl = new CommandLine(new OptionSetBuilder()
				.option("flag")
				.flag(true)
				.build(), "--flag");

		Assert.assertTrue(cl.optionSpecified("flag"));
		Assert.assertEquals("true", cl.getOptionValue("flag"));
		Assert.assertTrue(cl.getFlagValue("flag"));
	}

	@Test
	public void flagExplicitTrue() throws ParseException {
		CommandLine cl = new CommandLine(new OptionSetBuilder()
				.option("flag")
				.flag(true)
				.build(), "--flag", "true");

		Assert.assertTrue(cl.optionSpecified("flag"));
		Assert.assertEquals("true", cl.getOptionValue("flag"));
		Assert.assertTrue(cl.getFlagValue("flag"));
	}

	@Test
	public void flagExplicitFalse() throws ParseException {
		CommandLine cl = new CommandLine(new OptionSetBuilder()
				.option("flag")
				.flag(true)
				.build(), "--flag", "false");

		Assert.assertTrue(cl.optionSpecified("flag"));
		Assert.assertEquals("false", cl.getOptionValue("flag"));
		Assert.assertFalse(cl.getFlagValue("flag"));
	}

	@Test
	public void optionalMissing() throws ParseException {
		CommandLine cl = new CommandLine(new OptionSetBuilder()
				.option("flag")
				.optional(true)
				.flag(true)
				.build());

		Assert.assertFalse(cl.optionSpecified("flag"));
	}

	@Test
	public void notOptionalMissing() throws ParseException {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Required option missing 'flag'");

		CommandLine cl = new CommandLine(new OptionSetBuilder()
				.option("flag")
				.flag(true)
				.build());
	}
}
