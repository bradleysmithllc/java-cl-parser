package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class UsageFormatTest
{
	@Test
	public void wrapText()
	{
		List<String> res = StringUtil.wrapTextToList("Hello", 3);

		//Assert.assertEquals(2, res.size());
		Assert.assertEquals("He-", res.get(0));
		Assert.assertEquals("llo", res.get(1));
	}

	@Test
	public void wrapTextWithNewlines()
	{
		List<String> res = StringUtil.wrapTextToList("Hello\nWorld\n\nTill we meet again", 15);

		//Assert.assertEquals(2, res.size());
		Assert.assertEquals("Hello          ", res.get(0));
		Assert.assertEquals("World          ", res.get(1));
		Assert.assertEquals("               ", res.get(2));
		Assert.assertEquals("Till   we  meet", res.get(3));
		Assert.assertEquals("again          ", res.get(4));
	}

	@Test
	public void wrapTextExactMatchToWidth()
	{
		List<String> res = StringUtil.wrapTextToList("Helloo", 3);

		//Assert.assertEquals(3, res.size());
		Assert.assertEquals("He-", res.get(0));
		Assert.assertEquals("ll-", res.get(1));
		Assert.assertEquals("oo ", res.get(2));
	}

	@Test
	public void lineEndsWithPunctuation()
	{
		List<String> res = StringUtil.wrapTextToList("He. Me.", 3);

		Assert.assertEquals(2, res.size());
		Assert.assertEquals("He.", res.get(0));
		Assert.assertEquals("Me.", res.get(1));
	}

	@Test
	public void twoWordsOneGap()
	{
		List<String> res = StringUtil.wrapTextToList("He Meddd", 5);

		Assert.assertEquals(2, res.size());
		Assert.assertEquals("He   ", res.get(0));
		Assert.assertEquals("Meddd", res.get(1));
	}

	@Test
	public void wrapTextLotsJohnDonne()
	{
		List<String> res = StringUtil.wrapTextToList(
			"DEATH be not proud, though some have called thee Mighty and dreadfull, for, thou art not so, " +
			"For, those, whom thou think'st, thou dost overthrow, Die not, poore death, nor yet canst thou kill me.  " +
			"From rest and sleepe, which but thy pictures bee, Much pleasure, then from thee, much more must flow, " +
			"And soonest our best men with thee doe goe, Rest of their bones, and soules deliverie.  " +
			"Thou art slave to Fate, Chance, kings, and desperate men, And dost with poyson, warre, and sicknesse dwell, " +
			"And poppie, or charmes can make us sleepe as well, And better then thy stroake; why swell'st thou then; " +
			"One short sleepe past, wee wake eternally, And death shall be no more; death, thou shalt die.",
			40
		);

		Assert.assertEquals(18, res.size());
		Assert.assertEquals("DEATH  be  not  proud,  though some have", res.get(0));
		Assert.assertEquals("called  thee  Mighty and dreadfull, for,", res.get(1));
		Assert.assertEquals("thou  art  not so, For, those, whom thou", res.get(2));
		Assert.assertEquals("think'st,  thou dost overthrow, Die not,", res.get(3));
		Assert.assertEquals("poore death, nor yet canst thou kill me.", res.get(4));
		Assert.assertEquals("From  rest  and  sleepe,  which  but thy", res.get(5));
		Assert.assertEquals("pictures  bee,  Much pleasure, then from", res.get(6));
		Assert.assertEquals("thee,  much  more must flow, And soonest", res.get(7));
		Assert.assertEquals("our  best men with thee doe goe, Rest of", res.get(8));
		Assert.assertEquals("their  bones, and soules deliverie. Thou", res.get(9));
		Assert.assertEquals("art  slave  to  Fate, Chance, kings, and", res.get(10));
		Assert.assertEquals("desperate  men,  And  dost  with poyson,", res.get(11));
		Assert.assertEquals("warre,  and sicknesse dwell, And poppie,", res.get(12));
		Assert.assertEquals("or  charmes  can make us sleepe as well,", res.get(13));
		Assert.assertEquals("And   better   then   thy  stroake;  why", res.get(14));
		Assert.assertEquals("swell'st  thou  then;  One  short sleepe", res.get(15));
		Assert.assertEquals("past,  wee  wake  eternally,  And  death", res.get(16));
		Assert.assertEquals("shall be no more; death, thou shalt die.", res.get(17));
	}

	@Test(expected = IllegalArgumentException.class)
	public void wrapTextZero()
	{
		StringUtil.wrapTextToList("1", 0);
	}

	@Test
	public void emptyText()
	{
		List<String> res = StringUtil.wrapTextToList("", 5);
		Assert.assertEquals(1, res.size());
		Assert.assertEquals("     ", res.get(0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void oneCharacterWidth()
	{
		List<String> res = StringUtil.wrapTextToList("Hello World", 1);
	}

	@Test
	public void binaryContent()
	{
		List<String> res = StringUtil.wrapTextToList("1\r1\r1\t1\f1\b1\f1\b1\r1\r1\t1\u00001", 10);

		Assert.assertEquals(3, res.size());
		Assert.assertEquals("1  1  1  1", res.get(0));
		Assert.assertEquals("1\b1  1\b1 1", res.get(1));
		Assert.assertEquals("1 1\u00001     ", res.get(2));
	}
}