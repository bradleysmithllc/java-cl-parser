package org.bitbucket.bradleysmithllc.java_cl_parser;

import junit.framework.Assert;
import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class EnumArgsBadDefaultTest
{
	private String val;

	@CLIOption(
			name = "tc",
			valueType = CLIOption.value_type.required,
			enumeratedValues = {"yes", "no"},
			defaultValue = "sey"
	)
	public void setThreadCount(String ival)
	{
		Assert.assertEquals(val, ival);
	}

	@Test(expected = UsageException.class)
	public void argDefaultNotInEnumeration() throws Throwable
	{
		CommonsCLILauncher.mainWithInstance(this, new String[]{});
	}
}