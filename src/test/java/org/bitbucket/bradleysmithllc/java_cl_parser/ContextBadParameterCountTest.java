package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

@CLIEntry(
		nickName = "run",
		description = "I do lot's of things",
		version = "1.0"
)
public class ContextBadParameterCountTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@CLIOption(
				name = "tc",
				longName = "thread-count",
				description = "",
				required = true,
				valueType = CLIOption.value_type.required
		)
	public void setThreadCount(int count)
	{
	}

	@CLIContext
	public void context(CLParserContext context, int count)
	{
	}

	@CLIMain
	public void main()
	{
	}

	@Test
	public void testLongOptions() throws Throwable
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Context method must have exactly one parameter... public void org.bitbucket.bradleysmithllc.java_cl_parser.ContextBadParameterCountTest.context(org.bitbucket.bradleysmithllc.java_cl_parser.CLParserContext,int)");

		ContextBadParameterCountTest basicTest = new ContextBadParameterCountTest();
		CommonsCLILauncher.mainWithInstance(basicTest, new String[]{"--thread-count", "11"});
	}
}