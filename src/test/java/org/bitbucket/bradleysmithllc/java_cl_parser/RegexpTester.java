package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Assert;
import org.junit.Test;

public class RegexpTester
{
	@Test
	public void testExp()
	{
		String[] vals = CommonsCLILauncher.getArrayValue("a,b,c\\,d,\\{He,ll,o}", ',');

		Assert.assertNotNull(vals);
		Assert.assertEquals(4, vals.length);
		Assert.assertEquals("a", vals[0]);
		Assert.assertEquals("b", vals[1]);
		Assert.assertEquals("c,d", vals[2]);
		Assert.assertEquals("He,ll,o", vals[3]);
	}

	@Test
	public void testExp1()
	{
		String[] vals = CommonsCLILauncher.getArrayValue("\\{He,ll,o},a,b,c\\,d", ',');

		Assert.assertNotNull(vals);
		Assert.assertEquals(4, vals.length);
		Assert.assertEquals("He,ll,o", vals[0]);
		Assert.assertEquals("a", vals[1]);
		Assert.assertEquals("b", vals[2]);
		Assert.assertEquals("c,d", vals[3]);
	}

	@Test
	public void testExp2()
	{
		String[] vals = CommonsCLILauncher.getArrayValue("a,b,\\{He,ll,o},c\\,d", ',');

		Assert.assertNotNull(vals);
		Assert.assertEquals(4, vals.length);
		Assert.assertEquals("a", vals[0]);
		Assert.assertEquals("b", vals[1]);
		Assert.assertEquals("He,ll,o", vals[2]);
		Assert.assertEquals("c,d", vals[3]);
	}

	@Test
	public void testExp3()
	{
		String[] vals = CommonsCLILauncher.getArrayValue("a,b,\\\\{He,ll,o},c\\,d", ',');

		Assert.assertNotNull(vals);
		Assert.assertEquals(6, vals.length);
		Assert.assertEquals("a", vals[0]);
		Assert.assertEquals("b", vals[1]);
		Assert.assertEquals("\\{He", vals[2]);
		Assert.assertEquals("ll", vals[3]);
		Assert.assertEquals("o}", vals[4]);
		Assert.assertEquals("c,d", vals[5]);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testExp4()
	{
		CommonsCLILauncher.getArrayValue("a,\\b", ',');
	}

	@Test(expected = IllegalArgumentException.class)
	public void testExp5()
	{
		CommonsCLILauncher.getArrayValue("a,\\", ',');
	}

	@Test
	public void testExp6()
	{
		String[] vals = CommonsCLILauncher.getArrayValue("a\\\\b\\\\c\\\\d,e\\\\f\\\\g\\\\h", ',');

		Assert.assertNotNull(vals);
		Assert.assertEquals(2, vals.length);
		Assert.assertEquals("a\\b\\c\\d", vals[0]);
		Assert.assertEquals("e\\f\\g\\h", vals[1]);
	}

	@Test
	public void escapeFunction()
	{
		String[] vals = CommonsCLILauncher.getArrayValue("\\{a\\b/c\\d}", '/');

		Assert.assertNotNull(vals);
		Assert.assertEquals(1, vals.length);
		Assert.assertEquals("a\\b/c\\d", vals[0]);
	}

	@Test
	public void nestedEscapeFunctions()
	{
		String[] vals = CommonsCLILauncher.getArrayValue("\\{a\\b/c\\d\\{Hope}}", '/');

		Assert.assertNotNull(vals);
		Assert.assertEquals(1, vals.length);
		Assert.assertEquals("a\\b/c\\d\\{Hope}", vals[0]);
	}
}