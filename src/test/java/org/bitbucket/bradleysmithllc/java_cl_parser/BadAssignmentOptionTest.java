package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0",
		assignmentOrder = {"tc3"}
)
public class BadAssignmentOptionTest
{
	@CLIOption(
			name = "tc2",
			longName = "tc"
	)
	public void setThreadCount2(int count)
	{
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void duplicatedName() throws Throwable
	{
		BadAssignmentOptionTest cliLaunchable = new BadAssignmentOptionTest();
		CommonsCLILauncher.mainWithInstance(cliLaunchable, new String[]{});
	}
}