package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0"
)
public class DuplicatedOptionsTest
{
	@CLIOption(
			name = "tc"
	)
	public void setThreadCount(int count)
	{
	}

	@CLIOption(
			name = "tc"
	)
	public void setThreadCountDup(int count)
	{
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void duplicatedName() throws Throwable
	{
		DuplicatedOptionsTest cliLaunchable = new DuplicatedOptionsTest();
		CommonsCLILauncher.mainWithInstance(cliLaunchable, new String[]{});
	}
}