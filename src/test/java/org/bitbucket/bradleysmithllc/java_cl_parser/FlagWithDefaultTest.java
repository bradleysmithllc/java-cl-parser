package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Test;

@CLIEntry(
		nickName = "run",
		description = "I do lot's of things",
		version = "1.0"
)
public class FlagWithDefaultTest
{
	@CLIOption(
		name = "tc",
		description = "",
		valueType = CLIOption.value_type.flag,
		defaultValue = "tttt"
	)
	public void setThreadCount(String count)
	{
	}

	@CLIMain
	public void main()
	{
	}

	@Test(expected = InvalidCLIEntryException.class)
	public void testShortOptions() throws Throwable
	{
		CommonsCLILauncher.mainWithInstance(new FlagWithDefaultTest(), new String[]{});
	}
}