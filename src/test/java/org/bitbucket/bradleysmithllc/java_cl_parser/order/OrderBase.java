package org.bitbucket.bradleysmithllc.java_cl_parser.order;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

public class OrderBase
{
	protected String order = "";

	@CLIOption(
		name = "a",
		longName = "longa",
		required = true,
		valueType = CLIOption.value_type.required
	)
	public void setA(String val)
	{
		order += "a";
	}

	@CLIOption(
			name = "b",
			longName = "longb",
			required = true,
			valueType = CLIOption.value_type.required
	)
	public void setB(String val)
	{
		order += "b";
	}

	@CLIOption(
			name = "c",
			longName = "longc",
			required = true,
			valueType = CLIOption.value_type.required
	)
	public void setC(String val)
	{
		order += "c";
	}

	@CLIMain
	public void main()
	{
	}
}