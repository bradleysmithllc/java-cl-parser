package org.bitbucket.bradleysmithllc.java_cl_parser.order;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.junit.Assert;
import org.junit.Test;

@CLIEntry(
		description = "",
		version = "1.0",
		assignmentOrder = {"longc", "longa"}
)
public class PartialOrder2LongNamesTest extends OrderBase
{
	@Test
	public void names() throws Throwable
	{
		CommonsCLILauncher.mainWithInstance(this, new String[]{"-a", "1", "-b", "2", "-c", "3"});

		Assert.assertEquals("cab", order);
	}
}