package org.bitbucket.bradleysmithllc.java_cl_parser.util.test;

import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.java_cl_parser.util.RecursiveMap;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class RMapTest {
	@Test
	public void run()
	{
		Map<String, Object> smap = new HashMap<String, Object>();

		smap.put("hi", "hi");

		RecursiveMap rmap = new RecursiveMap(smap);

		Assert.assertEquals("hi", rmap.get("hi"));

		smap.put("hi2", "${hi}2");

		Assert.assertEquals("hi2", rmap.get("hi2"));

		smap.put("hi3", "${${hi}4}");
		smap.put("hi4", "hi4");

		Assert.assertEquals("hi4", rmap.get("hi3"));

		smap.put("hi5", 5);

		Assert.assertEquals(5, rmap.get("hi5"));

		smap.put("hi6", new StringBuffer("${hi}"));

		Assert.assertEquals("${hi}", rmap.get("hi6").toString());
	}

	@Test
	public void keysWithDollars()
	{
		Map<String, Object> smap = new HashMap<String, Object>();

		smap.put("$hi", "Dollar_hi");
		smap.put("$hi2", "${$hi}");

		smap.put("$$hi", "Dollar_hi");
		smap.put("$hi3", "${$$hi}");

		smap.put("$$POPULATION_DATE", "20121231");
		smap.put("$PMSessionLogFile", "s_m_EXTRACT_EMPDATA_SRT2IO8V66_${$$POPULATION_DATE}.log");

		smap.put("$PMRootDir", "\\\\edwfiles\\ETLDevFiles");
		smap.put("$PMBadFileDir", "${$PMRootDir}\\data\\BadFiles\\\\${$$POPULATION_DATE}");

		RecursiveMap rmap = new RecursiveMap(smap);

		Assert.assertEquals("Dollar_hi", rmap.get("$hi"));
		Assert.assertEquals("Dollar_hi", rmap.get("$hi3"));
		Assert.assertEquals("s_m_EXTRACT_EMPDATA_SRT2IO8V66_20121231.log", rmap.get("$PMSessionLogFile"));
		Assert.assertEquals("\\\\edwfiles\\ETLDevFiles\\data\\BadFiles\\20121231", rmap.get("$PMBadFileDir"));
	}

	@Test
	public void deepReplacement()
	{
		Map<String, Object> smap = new HashMap<String, Object>();

		for (int i = 0; i < 100; i++)
		{
			smap.put("TOKEN_" + i, "${TOKEN_" + (i + 1) + "}");
		}
		smap.put("TOKEN_100", "Grandioseness");

		RecursiveMap rmap = new RecursiveMap(smap);

		for (int i = 0; i < 100; i++)
		{
			Assert.assertEquals("Grandioseness", rmap.get("TOKEN_" + i));
		}
	}
}