package org.bitbucket.bradleysmithllc.java_cl_parser;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

@CLIEntry(
		description = "",
		version = "1.0",
		assignmentOrder = {"tc2", "tc3"}
)
public class DuplicatedOrdinalTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@CLIOption(
		name = "tc2",
		longName = "tc2",
		unnamedArgumentOrdinal = 1
	)
	public void setThreadCount2(int count)
	{
	}

	@CLIOption(
			name = "tc3",
			longName = "tc3",
			unnamedArgumentOrdinal = 1
	)
	public void setThreadCount3(int count)
	{
	}

	@Test
	public void dupOrdinal() throws Throwable
	{
		expectedException.expect(InvalidCLIEntryException.class);
		expectedException.expectMessage("Ordinal [1] specified more than once");

		DuplicatedOrdinalTest pot = new DuplicatedOrdinalTest();
		CommonsCLILauncher.mainWithInstance(pot, new String[]{});
	}
}